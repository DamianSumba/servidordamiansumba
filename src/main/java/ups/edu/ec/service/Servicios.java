package ups.edu.ec.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import ups.edu.ec.Bussiness.ProductoOn;
import ups.edu.ec.model.Carrito;
import ups.edu.ec.model.Producto;

@Path("/shop")
public class Servicios {
	
	@Inject
	private ProductoOn productoOn;
	
	@POST
    @Path("/insert")
    @Produces("application/json")
    public Response insertProducto(Producto p){
		Response.ResponseBuilder builder= null;
		Map<String, String> data = new HashMap<>();
        try {
        	productoOn.insertProducto(p);
        	builder =  Response.status(Response.Status.OK).entity(data);
		} catch (Exception e) {
			// TODO: handle exception
			builder =  Response.status(Response.Status.BAD_REQUEST).entity(data);
		}
		
	
	return builder.build();
	
	
	}
	
	@GET
	@Path("/listar")
	@Produces("application/json")
	public List<Producto> getProducto() {
		return productoOn.listarProducto();
		}
	
	@GET
	@Path("/listarc")
	@Produces("application/json")
	public List<Carrito> getCarritos() {
		return productoOn.listarCarrito();
		}
	
	@POST
    @Path("/insertc")
    @Produces("application/json")
    public Response insertCarrito(Carrito c){
		Response.ResponseBuilder builder= null;
		Map<String, String> data = new HashMap<>();
        try {
        	productoOn.insertCarrito(c);
        	builder =  Response.status(Response.Status.OK).entity(data);
		} catch (Exception e) {
			// TODO: handle exception
			builder =  Response.status(Response.Status.BAD_REQUEST).entity(data);
		}
		
	
	return builder.build();
	
	
	}
	
}
