package ups.edu.ec.Bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ups.edu.ec.Dao.ProductoDao;
import ups.edu.ec.model.Carrito;
import ups.edu.ec.model.Producto;

@Stateless
public class ProductoOn {
	
	@Inject		
	private ProductoDao dao;
	
	
	public boolean insertProducto (Producto p) throws Exception {
		return dao.insert(p);
	}
	
	public List<Producto> listarProducto (){
		return dao.listaProductos();
	}
	
	public List<Carrito> listarCarrito(){
		return dao.listaCarritos();
	}
	
	public boolean insertCarrito(Carrito p) throws Exception {
		return dao.insertCarrito(p);
	}
	

}
