package ups.edu.ec.Dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ups.edu.ec.model.Carrito;
import ups.edu.ec.model.Producto;

@Stateless
public class ProductoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public boolean insert(Producto p) throws Exception {
		try {
			em.persist(p);
			return true;
		} catch (Exception e) {
			throw new Exception("Error al ingresar producto: "+e.getMessage());
		}
	}
	
	public List<Producto> listaProductos (){
		
		String jpql = "select p from Producto p";
		
	Query q = em.createQuery(jpql, Producto.class);
	List<Producto> lista = q.getResultList();
	return lista;
		
	}
	
public List<Carrito> listaCarritos (){
		
		String jpql = "select c from Carrito c";
		
	Query q = em.createQuery(jpql, Producto.class);
	List<Carrito> lista = q.getResultList();
	return lista;
		
	}
	
	public boolean insertCarrito(Carrito c) throws Exception {
		
		try {
			em.persist(c);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Error al ingresar producto: "+e.getMessage());
		}
	}
	
	
	

}
