package ups.edu.ec.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class FacturaDetalle {
	@Id
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Producto producto;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Factura factura;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Carrito carrito;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Carrito getCarrito() {
		return carrito;
	}

	public void setCarrito(Carrito carrito) {
		this.carrito = carrito;
	}
	
	
	

}
